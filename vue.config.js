module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160071/learn_bootstrap/'
    : '/'
}
